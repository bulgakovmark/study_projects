package Sep_27.SSP;

public class Computer implements Methods {

    int points = 0;
    String action;

    @Override
    public String getName() {
        return "Computer";
    }

    @Override
    public String getMove() {
        String move = null;
        double rand = Math.random() * 10;
        if (rand <= 3) {
            move = "Stone";
        } else if (rand > 3 & rand <=6) {
            move = "Paper";
        } else if (rand > 6) {
            move = "Scissors";
        }
        action = move;
        return move;
    }

    @Override
    public void addPoint() {
        this.points += 1;
    }
}
