package Sep_27.SSP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game {

    Human human = new Human();
    Computer computer = new Computer();

    public void play() throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter your name:");
        human.setName(reader.readLine());

        while (true) {

            String pAction = human.getMove();
            String cAction = computer.getMove();

            if (pAction.equals("Stone") && cAction.equals("Stone")) {
                System.out.println("Draw!");
            }
            else if (pAction.equals("Stone") && cAction.equals("Paper")) {
                computer.addPoint();
                System.out.println("You lose!");
            }
            else if (pAction.equals("Stone") && cAction.equals("Scissors")) {
                human.addPoint();
                System.out.println("You win!");
            }
            else if (pAction.equals("Paper") && cAction.equals("Stone")) {
                human.addPoint();
                System.out.println("You win!");
            }
            else if (pAction.equals("Paper") && cAction.equals("Paper")) {
                System.out.println("Draw!");
            }
            else if (pAction.equals("Paper") && cAction.equals("Scissors")) {
                computer.addPoint();
                System.out.println("You lose!");
            }
            else if (pAction.equals("Scissors") && cAction.equals("Stone")) {
                computer.addPoint();
                System.out.println("You lose!");
            }
            else if (pAction.equals("Scissors") && cAction.equals("Paper")) {
                human.addPoint();
                System.out.println("You win!");
            }
            else if (pAction.equals("Scissors") && cAction.equals("Scissors")) {
                System.out.println("Draw!");
            }

            System.out.println("Computer choosed " + cAction);
            System.out.println(human.name + " " + human.points + " - " + computer.points + " " + computer.getName());
            System.out.println("Do you wanna continue? Yes/No:");

            if (reader.readLine().toUpperCase().equals("NO")) {
                break;
            }
        }

        System.out.println("Game over!");
        System.out.println(human.name + " " + human.points + " - " + computer.points + " " + computer.getName());
    }
}
