package Sep_27.SSP;

import java.io.IOException;

interface Methods {

    String getName() throws IOException;
    String getMove() throws IOException;
    void addPoint();
}
