package Sep_27.SSP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Human implements Methods{

    int points = 0;
    String name;
    String action;

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getMove() throws IOException {
        System.out.println("Choose your move 'Stone', 'Paper', 'Scissors':");
        action = reader.readLine();
        return action;
    }

    @Override
    public void addPoint() {
        this.points += 1;
    }
}
