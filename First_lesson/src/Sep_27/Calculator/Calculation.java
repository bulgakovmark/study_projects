package Sep_27.Calculator;

public abstract class Calculation {

    protected double x;
    protected double y;

    Calculation(double x, double y) {
        this.x = x;
        this.y = y;
    }

    abstract void print();
    abstract double execute();
}
