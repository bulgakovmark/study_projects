package Sep_27.Calculator;

public class DivideCalculation extends Calculation{

    DivideCalculation(double x, double y) {
        super(x, y);
    }
    @Override
    public double execute() {
        return x / y;
    }
    @Override
    public void print() {
        System.out.println("Пример: " + this.x + " / " + this.y);
    }
}
