package Sep_27.Calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class CalculationProvider {

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void getCalculation() throws IOException {

        System.out.println("Вас приветствует калькулятор!");

        System.out.println("Введите числа!");
        double x = Integer.parseInt(reader.readLine());
        double y = Integer.parseInt(reader.readLine());
        System.out.println("Ваши числа: х = " + x + ", у = " + y);

        System.out.println("Введите операцию (деление, умножение, сложение, вычетание)");
        String operation = reader.readLine();

        if (operation.equals("деление")) {
            DivideCalculation divideCalculation = new DivideCalculation(x, y);
            divideCalculation.print();
            System.out.println("Ответ: " + divideCalculation.execute());
        }
        else if (operation.equals("умножение")) {
            MultiplyCalculation multiplyCalculation = new MultiplyCalculation(x, y);
            multiplyCalculation.print();
            System.out.println("Ответ: " + multiplyCalculation.execute());
        }
        else if (operation.equals("сложение")) {
            AddCalculation addCalculation = new AddCalculation(x, y);
            addCalculation.print();
            System.out.println("Ответ: " + addCalculation.execute());
        }
        else if (operation.equals("вычетание")) {
            SubtractCalculation subtractCalculation = new SubtractCalculation(x, y);
            subtractCalculation.print();
            System.out.println("Ответ: " + subtractCalculation.execute());
        }
    }
}
