package Sep_27.Calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        CalculationProvider calculationProvider = new CalculationProvider();

        while(true) {

            calculationProvider.getCalculation();

            System.out.println("Посчитать еще что нибудь? Если нет, то введите 'Exit'");

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            if (reader.readLine().toUpperCase().equals("EXIT")) {
                break;
            }
        }
    }
}
