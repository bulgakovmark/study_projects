package Sep_27.Calculator;

public class MultiplyCalculation extends Calculation{

    MultiplyCalculation(double x, double y) {
        super(x, y);
    }
    @Override
    public double execute() {
        return x * y;
    }
    @Override
    public void print() {
        System.out.println("Пример: " + this.x + " * " + this.y);
    }
}
