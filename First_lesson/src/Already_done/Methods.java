package Already_done;

import java.util.ArrayList;
import java.util.Arrays;

public class Methods {

    public static void main(String[] args) {

        System.out.print(calculation(1, 3, 1));
    }

    public static void method1(int b) {
        System.out.println(b);
    }

    public static void method2(int b, int j) {
        for (int i = 0; i < j; i++) {
            System.out.print(b);
        }
        System.out.println();
    }

    public static void method3(int b, int j) {
        method2(b, j);
    }

    // квадрат числа
    public static double square(int num, int step) {
        double sqr = Math.pow(num, step);
        return sqr;
    }

    // значение линейной функции
    public static void fx1(double x, double k, double b) {
        double y = k * x + b;
        System.out.println(y);
    }

    // дискриминант
    public static double Dis(double a, double b, double c) {
        double D = Math.pow(b, 2) - 4 * a * c;
        return D;
    }

    // корни кв уравнения
    public static ArrayList<Double> calculation(double a, double b, double c) {

        ArrayList<Double> answer = new ArrayList<Double>();

        double D = Dis(a, b, c);

        if (D == 0) {
            answer.add((-b) / (2 * a));
        } else if (D > 0) {
            answer.add((-b - Math.sqrt(D)) / (2 * a));
            answer.add((-b + Math.sqrt(D)) / (2 * a));
        }
        return answer;
    }
}
