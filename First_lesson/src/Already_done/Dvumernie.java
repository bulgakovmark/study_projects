package Already_done;

import java.util.Arrays;

public class Dvumernie {
    public static void main(String[] args) {

        /* ЗАДАЧА 1

        int m = 5;
        int n = 7;

        int[][] ints = new int [m][n];
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++) {
                ints[i][j] = 1;
            }
            System.out.println(Arrays.toString(ints[i]));
        }
        */

        /* ЗАДАЧА 2

        int m = 3;
        int n = 3;
        int max = -10000;
        int sum = 0;
        int index = 1;

        int[][] ints = new int [m][n];
        for (int i = 0; i < m; i++){
            sum = 0;
            for (int j = 0; j < n; j++) {
                ints[i][j] = (-i) * (j ^ 2);
                sum = sum + ints[i][j];
            }
            System.out.println(Arrays.toString(ints[i]));

            if (sum > max){
                max = sum;
                index = i;
            }
        }
        System.out.println(max + ", " + Arrays.toString(ints[index]));

        */

        // ЗАДАЧА 3

        int n = 3;
        int ints[][] = new int[n][n];

        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                if (i == j){
                    ints[i][j] = 1;
                }
                else{
                    ints[i][j] = 0;
                }
            }
            System.out.println(Arrays.toString(ints[i]));
        }

    }
}
