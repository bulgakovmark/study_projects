package Already_done;

class Rectangle {
    private double height;
    private double width;

    static Rectangle getRectangle(double width, double height) {
        Rectangle result = new Rectangle();
        if (width > 0 & height > 0) {
            result.width = width;
            result.height = height;
        }
        else {
            System.out.println("Mistake!");
            System.exit(1);
        }
        return result;
    }

    static double getSquare(Rectangle rectangle) {
        return rectangle.width * rectangle.height;
    }

    static double getSquareForN(Rectangle[] rectangles, int index) {
        Rectangle rectangle = rectangles[index];
        return getSquare(rectangle);
    }

    static double getWidth(Rectangle rectangle){
        return rectangle.width;
    }

    static void setWidth(Rectangle rectangle, double width){
        rectangle.width = width;
    }
}
