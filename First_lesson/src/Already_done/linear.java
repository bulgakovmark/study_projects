package Already_done;

public class linear {
    public static void main(String[] args) {

        Equation equation = new Equation();
        equation.b = 3;
        equation.c =5;

        printEquation(equation);

        double answer = getPoint(equation, 5);
        System.out.print(answer);
    }

    static void printEquation(Equation equation){
        System.out.println("y = " + equation.b + "x + " + equation.c);
    }

    static double getPoint(Equation equation, double x){
        double result = equation.b * x + equation.c;
        return result;
    }

    static class Equation {
        double b;
        double c;
    }
}
