package Already_done;

public class Rectangle_Solution {
    public static void main(String[] args) {
        int n = 10;
        Rectangle[] rectangles = new Rectangle[n];

        rectangles[0] = Rectangle.getRectangle(0, 6);
        System.out.println(Rectangle.getSquare(rectangles[0]));

        System.out.println(Rectangle.getWidth(rectangles[0]));

        Rectangle.setWidth(rectangles[0], 100);
        System.out.println(Rectangle.getWidth(rectangles[0]));
    }


}
