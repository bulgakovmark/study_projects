package Already_done;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class task1 {
    public static void main(String[] args) throws IOException {

        // ОДНОМЕРНЫЕ МАССИВЫ

        /* ЗАДАЧА 1

        int[] ints = {4,4,4,14,4,4,4};
        boolean same = false;
        for (int i = 0; i < ints.length - 1;i++){
            if (ints[i] != ints[i+1]){
                break;
            }
            same = true;
        }
        System.out.println(same);
        */

        /* ЗАДАЧА 2

        int[] ints = {6,1,-4,0,3,2,2};
        int sum = 0;
        for (int i = 0; i < ints.length; i++){
            if (ints[i] > 0){
                sum += ints[i];
            }
        }
        System.out.println(sum);
        */

        /* Задача 3

        int [] ints= {1,2,3,5};
        boolean row = true;
        for (int i = 0; i < ints.length - 1; i++){
            if (ints[i] <= ints[i + 1]){
                row = false;
                break;
            }
        }
        System.out.println(row);
        */

        /* ЗАДАЧА 4

        int [] ints = {5,1,4,8,3,9,2,7};

        int max1 = ints[0];
        int max2 = ints[0];
        int max3 = ints[0];
        for (int i = 0; i < ints.length; i++){
            if (ints[i] > max1){
                max1 = ints[i];
            }
        }
        for (int i = 0; i < ints.length; i++){
            if (ints[i] >= max2 && ints[i] != max1){
                max2 = ints[i];
            }
        }
        for (int i = 0; i < ints.length; i++){
            if (ints[i] >= max3 && ints[i] != max1 && ints[i] != max2){
                max3 = ints[i];
            }
        }
        System.out.println(max1);
        System.out.println(max2);
        System.out.println(max3);
        */


        /* ЗАДАЧА 5

        int [] ints = {5,1,2,3};
        for (int i = 0; i < ints.length / 2; i++){
            int temporary = ints[i];
            ints[i] = ints[ints.length - 1 - i];
            ints[ints.length - 1 - i] = temporary;
        }
        System.out.println(Arrays.toString(ints));
        */

        /* ЗАДАЧА 6

        int [] ints = {1,5,3,7};
        for (int i = 0; i < ints.length; i ++){
            int temp = ints[i];
            ints[i] = ints[ints.length - 1];
            ints[ints.length - 1] = temp;
        }
        System.out.println(Arrays.toString(ints));
        */
    }
}
