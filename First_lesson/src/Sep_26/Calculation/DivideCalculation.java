package Sep_26.Calculation;

public class DivideCalculation extends AddCalculation{

    DivideCalculation(double x, double y) {
        super(x, y);
    }

    @Override
    public double execute() {
        return x / y;
    }

    @Override
    public void print() {
        System.out.println(this.x + " / " + this.y);
    }
}
