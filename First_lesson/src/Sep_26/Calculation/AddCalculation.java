package Sep_26.Calculation;

public class AddCalculation {

    protected double x;
    protected double y;

    AddCalculation(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double execute() {
        return this.x + this.y;
    }

    public void print() {
        System.out.println(this.x + " + " + this.y);
    }
}
