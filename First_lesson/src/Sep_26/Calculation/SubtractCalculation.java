package Sep_26.Calculation;

public class SubtractCalculation extends AddCalculation{

    SubtractCalculation(double x, double y) {
        super(x, y);
    }

    @Override
    public double execute() {
        return x - y;
    }

    @Override
    public void print() {
        System.out.println(this.x + " - " + this.y);
    }
}
