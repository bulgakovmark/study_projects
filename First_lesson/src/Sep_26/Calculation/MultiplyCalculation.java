package Sep_26.Calculation;

public class MultiplyCalculation extends AddCalculation{

    MultiplyCalculation(double x, double y) {
        super(x, y);
    }

    @Override
    public double execute() {
        return x * y;
    }

    @Override
    public void print() {
        System.out.println(this.x + " * " + this.y);
    }
}
