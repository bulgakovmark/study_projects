package Sep_26.Human;

public class Human {

    private String name;

    public Human(String name) {
        this.name = name;
    }

    public void sayHello() {
        System.out.println("Hi my name is " + name + ".");
    }
}
