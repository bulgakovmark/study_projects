package Sep_25;

import com.sun.org.apache.regexp.internal.RE;

public class Main {
    public static void main(String[] args) {
        // Rectangles[] rectangles = {Rectangle.getRectangle(2,5)...}
        Rectangle shapes[] = new Rectangle[10];
        Square squares[] = new Square[10];

        for (int i = 0; i < shapes.length; i++) {
            shapes[i] = new Rectangle(i + 1, i + 2);
            squares[i] = new Square(i + 1);
            System.out.println("Rectangle " + (i + 1) + ")" + shapes[i].get_S());
            System.out.println("Square " + (i + 1) + ")" + squares[i].get_S_kv());
        }
        System.out.println("_____________________________________");
        System.out.println("Сумма площадей равна: " + getSum(shapes, squares));
        System.out.println("Максимальная площадь равна: " + getMax(shapes,squares));
        System.out.println("Минимальная площадь равна: " + getMin(shapes, squares));
    }

    public static double getSum(Rectangle shapes[], Square squares[]) {
        double sum = 0;
        for (int i = 0; i < shapes.length; i++) {
            sum += shapes[i].get_S();
        }
        for (int i = 0; i < squares.length; i++) {
            sum += squares[i].get_S_kv();
        }
        return sum;
    }

    public static double getMax(Rectangle shapes[], Square squares[]) {
        double max = shapes[0].get_S();
        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i].get_S() > max) {
                max = shapes[i].get_S();
            }
        }
        for (int i = 0; i < squares.length; i++) {
            if (squares[i].get_S_kv() > max) {
                max = squares[i].get_S_kv();
            }
        }
        return max;
    }

    public static double getMin(Rectangle shapes[], Square squares[]) {
        double min = shapes[0].get_S();
        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i].get_S() < min) {
                min = shapes[i].get_S();
            }
        }
        for (int i = 0; i < shapes.length; i++) {
            if (squares[i].get_S_kv() < min) {
                min = squares[i].get_S_kv();
            }
        }
        return min;
    }
}
