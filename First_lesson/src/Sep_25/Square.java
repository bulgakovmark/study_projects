package Sep_25;

public class Square {

    double side;

    Square(double side){
        this.side = side;
    }

    public double get_S_kv(){
        return Math.pow(side, 2);
    }
}
