
public class NameGoodComparator implements GoodComparator {
    @Override
    public int compare(Good first, Good second) {
        return first.name.compareTo(second.name);
    }
}
