import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        /*Good[] goods = new Good[]{
                new Computer("ASUS", 100),
                new Computer("ACER", 50),
                new Monitor("ViewSonic", 20, 14)
        };

        Good max = findGoodWithMaxPrice(goods);
        System.out.println(max);*/

        /*Monitor[] monitors = new Monitor[]{
                new Monitor("1", 10, 14),
                new Monitor("2", 20, 15),
                new Monitor("3", 30, 25),
        };

        Good max = findGoodWithMaxPrice(monitors);

        System.out.println(max);*/

        /* Monitor[] monitors = new Monitor[]{
                new Monitor("1", 10, 14),
                new Monitor("2", 20, 15),
                new Monitor("3", 30, 25),
        };

        Good[] goods = monitors;

        goods[0] = new Computer("ASUS", 111); */

        /*Good[] goods = new Good[]{
                new Computer("ASUS", 100),
                new Computer("ACER", 50),
                new Monitor("ViewSonic", 20, 111)
        };

        Monitor[] monitors = (Monitor[]) goods;*/

        /* ArrayList<Good> list = new ArrayList<Good>();
        list.add(new Computer("ASUS", 100));
        list.add(new Computer("ACER", 50));
        list.add(new Monitor("ViewSonic", 20, 14));

        Good max = finGoodWithMaxPrice(list);
        System.out.println(max); */

        /*ArrayList<Monitor> list = new ArrayList<Monitor>();
        list.add(new Monitor("1", 10, 14));
        list.add(new Monitor("2", 20, 15));
        list.add(new Monitor("3", 30, 25));

        Good max = finGoodWithMaxPrice(list);
        System.out.println(max);*/

        /*ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);

        ArrayList<Integer> listDest = new ArrayList<Integer>();

        System.out.println(listDest);
        addAll(list, listDest);
        System.out.println(listDest); */

        /*ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);

        ArrayList<Number> listDest = new ArrayList<Number>();

        System.out.println(listDest);
        addAll(list, listDest);
        System.out.println(listDest);*/

        /*ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);

        ArrayList<Object> listDest = new ArrayList<Object>();

        System.out.println(listDest);
        addAll(list, listDest);
        System.out.println(listDest);*/

        //ComputerBox<Computer> box = new ComputerBox<Computer>();

        ArrayList<Monitor> list = new ArrayList<Monitor>();
        list.add(new Monitor("b", 10, 14));
        list.add(new Monitor("a", 20, 15));
        list.add(new Monitor("c", 30, 25));

        Good result = getMin(list, new NameGoodComparator());
        System.out.println(result);
    }

    public static Good findGoodWithMaxPrice(Good[] goods){
        Good max = goods[0];

        for (int i = 1; i < goods.length; i++) {
            if (goods[i].price > max.price) {
                max = goods[i];
            }
        }

        return max;
    }

    public static Good finGoodWithMaxPrice(ArrayList<? extends Good> goods) {
        Good result = goods.get(0);

        for (int i = 1; i < goods.size(); i++) {
            if(goods.get(i).price > result.price){
                result = goods.get(i);
            }
        }

        return result;
    }

    public static Good finGoodWithMinPrice(ArrayList<? extends Good> goods) {
        Good result = goods.get(0);

        for (int i = 1; i < goods.size(); i++) {
            if(goods.get(i).price < result.price){
                result = goods.get(i);
            }
        }

        return result;
    }

    public static Good finGoodWithMinName(ArrayList<? extends Good> goods) {
        Good result = goods.get(0);

        for (int i = 1; i < goods.size(); i++) {
            if(goods.get(i).name.compareTo(result.name) == -1){
                result = goods.get(i);
            }
        }

        return result;
    }

    public static Good getMin(ArrayList<? extends Good> goods, GoodComparator comparator) {
        Good result = goods.get(0);

        for (int i = 1; i < goods.size(); i++) {
            if (comparator.compare(goods.get(i), result) == -1) {
                result = goods.get(i);
            }
        }

        return result;
    }

    public static void addGoodToList(Good good, ArrayList<? super Good> goods){
        goods.add(good);
    }

    public static void addInteger(Integer i, List<? super Integer> list){
        list.add(i);
    }

    public static <T> void addAll(List<? extends T> source, List<? super T> dest){
        for (int i = 0; i < source.size(); i++) {
            dest.add(source.get(i));
        }
    }
}