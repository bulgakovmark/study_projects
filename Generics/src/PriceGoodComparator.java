
public class PriceGoodComparator implements GoodComparator {
    public int compare(Good first, Good second){
        if (first.price == second.price){
            return 0;
        }
        if (first.price > second.price) {
            return 1;
        }
        return -1;
    }
}
