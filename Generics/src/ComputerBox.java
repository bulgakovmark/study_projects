
public class ComputerBox<T extends Computer> {
    private T content;

    public void put(T content){
        this.content = content;
    }

    public T get(){
        return this.content;
    }

    public int getRamSize(){
        return this.content.ram;
    }
}
