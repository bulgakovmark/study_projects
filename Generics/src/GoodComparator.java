
public interface GoodComparator {
    int compare(Good first, Good second);
}
