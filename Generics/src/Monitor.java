
public class Monitor extends Good {
    int screenSize;

    public Monitor(String name, double price, int screenSize) {
        super(name, price);
        this.screenSize = screenSize;
    }
}
