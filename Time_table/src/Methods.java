import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

class Methods {

    //
    void set(String startTime, String endTime, ArrayList<ArrayList<Date>> list) throws ParseException {

        ArrayList<Date> timeLines = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date start = format.parse(startTime);
        Date end = format.parse(endTime);
        timeLines.add(start);
        timeLines.add(end);

        // checks if 'end' time is before 'start' time. Also don't allows to create a meeting after midnight
        if (start.compareTo(end) > 0 || start.compareTo(end) == 0) {
            System.out.println("Wrong input");
            return;
        }

        // compares if new time period is inside of existing periods
        for (int i = 0; i < list.size(); i++) {
            if ((start.compareTo(list.get(i).get(0)) > 0 && start.compareTo(list.get(i).get(1)) < 0) ||
                    (end.compareTo(list.get(i).get(0)) > 0 && end.compareTo(list.get(i).get(1)) < 0) ||
                    (start.compareTo(list.get(i).get(0)) == 0 && end.compareTo(list.get(i).get(1)) == 0)) {
                System.out.println("The time is not available");
                return;
            }
        }
        list.add(timeLines);

        // bubble sort of the array
        ArrayList<Date> temp;
        for (int i = 0; i < list.size(); i++) {
            for (int j = 1; j < list.size() - i; j++) {
                if (list.get(j - 1).get(0).after(list.get(j).get(0))) {
                    temp = list.get(j - 1);
                    list.set(j - 1, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        System.out.println("OK");
    }

    // prints all elements of the array from a new line
    void print(ArrayList<ArrayList<Date>> list) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        System.out.println("Your time-table for today");
        for (ArrayList<Date> element : list) {
            System.out.println(format.format(element.get(0)) + " .. " + format.format(element.get(1)));
        }
    }

    // finds earliest position in the entered period with 'duration'
    void findFirst(String startTime, String endTime, String duration, ArrayList<ArrayList<Date>> list) throws ParseException, IOException {

        SimpleDateFormat dformat = new SimpleDateFormat("HH:mm");
        Date start = dformat.parse(startTime);
        Date end = dformat.parse(endTime);
        Date durationD = dformat.parse(duration);

        // if list is empty, just add in the beginning of a period
        if (list.size() == 0) {
            System.out.println(dformat.format(start) + " " + dformat.format(sumTimes(start, durationD)));
            wantToSet(startTime, dformat.format(sumTimes(start, durationD)), list);
            return;
        }

        // if list consists of 1 element. Checks if can add an element to the beginning of a time-table
        if (!(start.compareTo(list.get(0).get(0)) > 0 && start.compareTo(list.get(0).get(1)) < 0) &&
                !(sumTimes(start, durationD).compareTo(list.get(0).get(0)) > 0 && sumTimes(start, durationD).compareTo(list.get(0).get(1)) < 0) &&
                !(start.compareTo(list.get(0).get(0)) == 0 && sumTimes(start, durationD).compareTo(list.get(0).get(1)) == 0)) {
            System.out.println(dformat.format(start) + " " + dformat.format(sumTimes(start, durationD)));
            wantToSet(startTime, dformat.format(sumTimes(start, durationD)), list);
            return;
        }

        // else situations
        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1) {
                // checks if can add to the end of a time-table
                if (sumTimes(list.get(i).get(1), durationD).getTime() <= end.getTime()) {
                    System.out.println(dformat.format(list.get(i).get(1)) + " " + dformat.format(sumTimes(list.get(i).get(1), durationD)));
                    wantToSet(dformat.format(list.get(i).get(1)), dformat.format(sumTimes(list.get(i).get(1), durationD)), list);
                    return;
                } else {
                    System.out.println("No time available");
                }
                // checks if can insert a meeting between existing meetings
            } else if ((list.get(i + 1).get(0).getTime() - list.get(i).get(1).getTime()) >= Math.abs(durationD.getTime()) &&
                    list.get(i).get(1).compareTo(start) > 0 && list.get(i).get(1).compareTo(end) < 0) {
                System.out.println(dformat.format(list.get(i).get(1)) + " " + dformat.format(sumTimes(list.get(i).get(1), durationD)));
                wantToSet(dformat.format(list.get(i).get(1)), dformat.format(sumTimes(list.get(i).get(1), durationD)), list);
                return;
            }
        }
    }

    // returns a sum of two Date objects
    private Date sumTimes(Date time1, Date time2) throws ParseException {
        Date newTime;
        Calendar calendarnewTime = Calendar.getInstance();
        calendarnewTime.setTime(time1);
        calendarnewTime.add(Calendar.HOUR_OF_DAY, time2.getHours());
        calendarnewTime.add(Calendar.MINUTE, time2.getMinutes());
        newTime = calendarnewTime.getTime();
        return newTime;
    }

    // offers to set a new meeting to the list
    private void wantToSet(String time1, String time2, ArrayList<ArrayList<Date>> list) throws IOException, ParseException {
        System.out.println("Do you want to set a recommendation? (yes / no)");
        if (new BufferedReader(new InputStreamReader(System.in)).readLine().toLowerCase().equals("yes")) {
            set(time1, time2, list);
        }
    }
}
