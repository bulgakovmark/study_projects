import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class Time_table {

    public static void main(String[] args) throws ParseException, IOException {
        Methods methods = new Methods();

        ArrayList<ArrayList<Date>> timeTable = new ArrayList<>();

        System.out.println("Welcome to your time-table");
        System.out.println("------------------------------------------");
        System.out.println("* use 'set' to set a meeting");
        System.out.println("* use 'print' to print planed meetings");
        System.out.println("* use 'findfirst' find earliest time for your meeting");
        System.out.println("------------------------------------------");

        while (true) {

            String[] words = new BufferedReader(new InputStreamReader(System.in)).readLine().trim().split("\\s+");

            if (words[0].equals("exit")) {
                break;
            }
            String startTime, endTime, duration;

            startTime = words[1];
            endTime = words[2];

            if (words[0].toLowerCase().equals("set")) {
                methods.set(startTime, endTime, timeTable);
            }
            if (words[0].toLowerCase().equals("print")) {
                methods.print(timeTable);
            }
            if (words[0].toLowerCase().equals("findfirst")) {
                duration = words[3];
                methods.findFirst(startTime, endTime, duration, timeTable);
            }
        }
    }
}